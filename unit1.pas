unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnAdd: TBitBtn;
    btnRemove: TBitBtn;
    Label1: TLabel;
    lblTime: TLabel;
    lblDate: TLabel;
    lstAlarms: TListBox;
    Timer1: TTimer;
    TrayIcon1: TTrayIcon;
    procedure btnAddClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormWindowStateChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

uses
  Unit2;

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnAddClick(Sender: TObject);
var
  AlarmText: string;
begin
  if Form2.ShowModal = mrOK then begin
    AlarmText := Form2.lstHour.GetSelectedText + ':'
        + Form2.lstMinute.GetSelectedText + ' ' + Form2.lstampm.GetSelectedText
        + ' - ' + Form2.edtHint.Text;
    lstAlarms.Items.Add(AlarmText);
  end;
end;

procedure TForm1.btnRemoveClick(Sender: TObject);
begin
  lstAlarms.DeleteSelected;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // Save alarm list to file alarmlist.dat on the same directory as the executable/exe
  lstAlarms.Items.SaveToFile(Application.Location + DirectorySeparator + 'alarmlist.dat');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Load alarm list from alarmlist.dat file
  if FileExists(Application.Location + DirectorySeparator + 'alarmlist.dat') then
    lstAlarms.Items.LoadFromFile(Application.Location + DirectorySeparator + 'alarmlist.dat');

  Timer1Timer(Sender);
end;

procedure TForm1.FormWindowStateChange(Sender: TObject);
begin
  // If minimized, make form invisible
  Visible := not (WindowState = wsMinimized);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  i: Integer;
  TimeStamp: string;
  AlarmData: TStringArray;
begin
  // Show current time
  lblTime.Caption:=FormatDateTime('hh:nn am/pm', Time);
  // Show current date
  lblDate.Caption:=FormatDateTime('ddd, d mmm yyyy', Date);

  // Get the current timestamp to compare with each alarm time
  TimeStamp := FormatDateTime('hh:nn am/pm', Time);
  // Loop through the alarm list to see if it's time to ring the alarm!
  for i := 0 to lstAlarms.Items.Count - 1 do begin
    // Check if the list item exists. We have to check this in case any previous
    // alarm triggered and some list item removed.
    if i < lstAlarms.Items.Count then begin
      AlarmData := lstAlarms.Items[i].Split('-');
      //ShowMessage(Trim(AlarmData[0]) + ' - ' + TimeStamp);
      if Trim(AlarmData[0]) = TimeStamp then begin
        // We remove the alarm, so that it doesn't ring again
        lstAlarms.Items.Delete(i);
        // We show the alarm message
        ShowMessage('Alarm rings!'#13#10#13#10
                    + 'Alarm time: ' + AlarmData[0] + #13#10
                    + 'Alarm hint: ' + AlarmData[1]);
      end;
    end;
  end;
end;

procedure TForm1.TrayIcon1Click(Sender: TObject);
begin
  // Restore from minimized state when tray icon is clicked
  WindowState := wsNormal;
  // Above command doesn't make the form visible, so we need to make it visible
  Visible := True;
end;

end.


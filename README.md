# Alarm Clock Advanced

An advanced alarm clock project with multi-alarm, alarm memory, alarm hint, easy input and tray icon features

Made as a part of an article for LazPlanet.

![Alarm clock main window](https://gitlab.com/lazplanet/alarm-clock-advanced/-/raw/main/images/screenshot-01.png "Alarm clock main window")

![Alarm clock creation window](https://gitlab.com/lazplanet/alarm-clock-advanced/-/raw/main/images/screenshot-02.png "Alarm clock creation window")

## Features

- Supports multiple alarms
- Remembers your last saved alarms everytime you launch
- Has alarm hint - to remind you for which you set the alarm
- Input is easy - can set an alarm without touching the keyboard; similar to mobile UI
- Tray icon - hides itself when minimized but still has a tray icon to restore when needed
- Everything is CC0 - so you can use this, modify this for any purpose, without any attribution or anything

## Run

Just open the .lpi file in Lazarus IDE and click Run - Run or press F9.
